package example.com.prac9_1;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements Runnable {
    int tmp;
    private MediaPlayer mediaPlayer = null;
    private ProgressBar mProgress;
    SeekBar mSeekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initMP();


    }
    @Override
    protected void onDestroy() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onDestroy();
    }

    public void initMP(){
        mediaPlayer = MediaPlayer.create(this, R.raw.music);
        mSeekBar = (SeekBar)findViewById(R.id.seekBar);
        mProgress = (ProgressBar)findViewById(R.id.progressBar1);
        mProgress.setProgress(0);
        mProgress.setMax(0);

        Thread thrd = new Thread(this);
        thrd.setDaemon(true);

        mSeekBar.setMax(mediaPlayer.getDuration());
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tmp = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(tmp);
            }
        });
        thrd.start();

    }
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                showProgress();
            }
        }
    };

    public void startMP(View v){
        Button btn;
        btn = (Button)findViewById(R.id.button0); // 각 버튼의 상황별 Enable 결정
        btn.setEnabled(false);
        btn = (Button)findViewById(R.id.button1); // 각 버튼의 상황별 Enable 결정
        btn.setEnabled(true);
        btn = (Button)findViewById(R.id.button2);
        btn.setEnabled(true);
        mProgress.setProgress(0);

        mProgress.setMax(mediaPlayer.getDuration());
        mediaPlayer.start();            //재생
        mediaPlayer.setLooping(true);   //반복재생
    }

    public void pauseMP(View v) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            TextView output = (TextView)findViewById(R.id.textView1);
            output.setText("일시정지");
        } else {
            mediaPlayer.start();
        }
    }
    @Override
    public void run() {
        while (mediaPlayer != null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
            mHandler.sendMessage(Message.obtain(mHandler, 1));
        }
    }

    public void showProgress() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mProgress.setProgress(mediaPlayer.getCurrentPosition());
            mSeekBar.setProgress(mediaPlayer.getCurrentPosition());
            TextView output = (TextView)findViewById(R.id.textView1);
            output.setText(
                    "재생 중"
                            + "\n현재 위치: " + mediaPlayer.getCurrentPosition() / 1000
                            + "\n전체 길이: " + mediaPlayer.getDuration() / 1000 );
        }
    }
    public void stopMP(View v) {
        mediaPlayer.stop();
        initMP();
        Button btn;
        btn = (Button)findViewById(R.id.button0);
        btn.setEnabled(true);
        btn = (Button)findViewById(R.id.button1);
        btn.setEnabled(false);
        btn = (Button)findViewById(R.id.button2);
        btn.setEnabled(false);
        mProgress.setProgress(0);
        mProgress.setMax(mediaPlayer.getDuration());
        TextView output = (TextView)findViewById(R.id.textView1);
        output.setText("중지");
    }
}
